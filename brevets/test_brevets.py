import nose
import acp_times

def test_brevets():
    """
    Testing different brevet times
    """
    #standard nosetsest
    assert acp_times.close_time(60, 200, "2017-01-01T00:00:00+00:00") == "2017-01-01T04:00:00+00:00"
    assert acp_times.close_time(200, 200, "2017-01-01T00:00:00+00:00") == "2017-01-01T13:30:00+00:00"
    #testing opening time algo
    assert acp_times.open_time(350, 400, "2017-01-01T00:00:00+00:00") == "2017-01-01T10:34:00+00:00"
    assert acp_times.open_time(890, 1000, "2017-01-01T00:00:00+00:00") == "2017-01-02T05:09:00+00:00"
    #testing closing time algo
    assert acp_times.close_time(890, 1000, "2017-01-01T00:00:00+00:00") == "2017-01-03T17:23:00+00:00"
    #testing oddities case 
    assert acp_times.close_time(20, 200, "2017-01-01T00:00:00+00:00") == "2017-01-01T02:00:00+00:00"
    assert acp_times.open_time(20, 200, "2017-01-01T00:00:00+00:00") == "2017-01-01T00:35:00+00:00"
    #testing brevet within 20%
    assert acp_times.close_time(480, 400, "2017-01-01T00:00:00+00:00") == "2017-01-02T03:00:00+00:00"
